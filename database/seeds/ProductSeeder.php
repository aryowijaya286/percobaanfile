<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
        	'nama' => 'AREMA FC HOME JERSEY 2021 ',
            'liga_id' => 1,
            'gambar' => 'HOME AREMA FC 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'BALI UNITED HOME JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'HOME BALI UNITED 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'BHAYANGKARA FC HOME JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'HOME BHAYANGKARA FC 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'BHAYANGKARA FC AWAY JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'AWAY BHAYANGKARA FC 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'BHAYANGKARA FC THIRD JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'THIRD BHAYANGKARA FC 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'BORNEO FC HOME JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'HOME BORNEO 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'BORNEO FC AWAY JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'AWAY BORNEO 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'BORNEO FC GK JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'GK BORNEO 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'MADURA UNITED HOME JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'HOME MADURA UNITED 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'MADURA UNITED AWAY JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'AWAY MADURA UNITED 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'MADURA UNITED THIRD JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'THIRD MADURA UNITED 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'MADURA UNITED GK HOME JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'GK HOME MADURA UNITED 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'MADURA UNITED GK AWAY JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'GK AWAY MADURA UNITED 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'MADURA UNITED GK THIRD JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'GK THIRD MADURA UNITED 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'MADURA UNITED HOME JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'HOME MADURA UNITED 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'MADURA UNITED HOME JERSEY PIALA MENPORA 2021',
            'liga_id' => 1,
            'gambar' => 'HOME PIALA MENPORA MADURA UNITED 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'MADURA UNITED THIRD JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'THIRD MADURA UNITED 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'MADURA UNITED AWAY PRAMUSIM JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'AWAY PRAMUSIM MADURA UNITED 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSEBAYA SURABAYA HOME JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'HOME PERSEBAYA 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSEBAYA SURABAYA AWAY JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'AWAY PERSEBAYA 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSEBAYA SURABAYA PIALA MENPORA HOME JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'HOME PIALA MENPORA PERSEBAYA 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSEBAYA SURABAYA PIALA MENPORA AWAY JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'AWAY PIALA MENPORA PERSEBAYA 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSEBAYA SURABAYA PIALA MENPORA THIRD JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'THIRD PIALA MENPORA PERSEBAYA 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSELA LAMONGAN HOME JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'HOME PERSELA 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSELA LAMONGAN AWAY JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'AWAY PERSELA 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSELA LAMONGAN THIRD JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'THIRD PERSELA 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIB BANDUNG HOME JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'HOME PERSIB 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIB BANDUNG AWAY JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'AWAY PERSIB 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIB BANDUNG THIRD JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'THIRD PERSIB 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIB BANDUNG GK JERSEY 2019',
            'liga_id' => 1,
            'gambar' => 'GK PERSIB 2019.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIB BANDUNG GK AWAY JERSEY 2018',
            'liga_id' => 1,
            'gambar' => 'GK AWAY PERSIB 2018.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIJA JAKARTA HOME JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'HOME PERSIJA 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIJA JAKARTA AWAY JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'AWAY PERSIJA 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIJA JAKARTA THIRD JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'THIRD PERSIJA 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIJA JAKARTA GK HOME JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'GK HOME PERSIJA 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIJA JAKARTA GK AWAY JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'GK AWAY PERSIJA 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIJA JAKARTA GK THIRD JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'GK THIRD PERSIJA 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIK KEDIRI HOME JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'HOME PERSIK 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIK KEDIRI AWAY JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'AWAY PERSIK 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIK KEDIRI PIALA MENPORA HOME JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'HOME PIALA MENPORA PERSIK 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIK KEDIRI PIALA MENPORA AWAY JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'AWAY PIALA MENPORA PERSIK 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIK KEDIRI PIALA MENPORA THIRD JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'THIRD PIALA MENPORA PERSIK 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIK KEDIRI PIALA MENPORA GK HOME JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'GK HOME PIALA MENPORA PERSIK 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIK KEDIRI PIALA MENPORA GK AWAY JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'GK AWAY PIALA MENPORA PERSIK 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIK KEDIRI PIALA MENPORA GK THIRD JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'GK THIRD PIALA MENPORA PERSIK 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIKABO BOGOR 1973 HOME JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'HOME PERSIKABO 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIKABO BOGOR 1973 AWAY JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'AWAY PERSIKABO 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIPURA JAYAPURA AFC CUP HOME JERSEY 2015',
            'liga_id' => 1,
            'gambar' => 'HOME AFC CUP PERSIPURA 2015.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIPURA JAYAPURA HOME JERSEY NEW ERA',
            'liga_id' => 1,
            'gambar' => 'HOME PERSIPURA.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIRAJA BANDA ACEH HOME JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'HOME PERSIRAJA 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIRAJA BANDA ACEH AWAY JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'AWAY PERSIRAJA 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIRAJA BANDA ACEH HOME JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'HOME PERSIRAJA 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIRAJA BANDA ACEH AWAY JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'AWAY PERSIRAJA 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIRAJA BANDA ACEH THIRD JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'THIRD PERSIRAJA 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIRAJA BANDA ACEH GK HOME JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'GK HOME PERSIRAJA 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIRAJA BANDA ACEH GK AWAY JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'GK AWAY PERSIRAJA 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSITA TANGERANG HOME JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'HOME PERSITA 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSITA TANGERANG AWAY JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'AWAY PERSITA 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSITA TANGERANG THIRD JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'THIRD PERSITA 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSITA TANGERANG GK HOME JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'GK HOME PERSITA 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSITA TANGERANG GK AWAY JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'GK AWAY PERSITA 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'BARITO PUTERA HOME NEW ERA JERSEY',
            'liga_id' => 1,
            'gambar' => 'HOME BARITO.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSIS SEMARANG HOME JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'HOME PSIS 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSIS SEMARANG GK JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'GK PSIS 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSIS SEMARANG HOME JERSEY',
            'liga_id' => 1,
            'gambar' => 'HOME PSIS.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSIS SEMARANG AWAY JERSEY',
            'liga_id' => 1,
            'gambar' => 'AWAY PSIS.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSIS SEMARANG PIALA MENPORA HOME JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'HOME PIALA MENPORA PSIS 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSIS SEMARANG PIALA MENPORA THIRD JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'THIRD PIALA MENPORA PSIS 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSM MAKASAR HOME JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'HOME PSM 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSM MAKASAR AWAY JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'AWAY PSM 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSM MAKASAR GK JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'GK PSM 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSS SLEMAN HOME JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'HOME PSS 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSS SLEMAN AWAY JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'AWAY PSS 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSS SLEMAN THIRD JERSEY 2020',
            'liga_id' => 1,
            'gambar' => 'THIRD PSS 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSS SLEMAN THIRD JERSEY 2021',
            'liga_id' => 1,
            'gambar' => 'THIRD PSS 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'AHHA PS PATI JERSEY TRAINING COLOR BLACK',
            'liga_id' => 2,
            'gambar' => 'TRAINING BLACK AHHA PS PATI.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'AHHA PS PATI JERSEY TRAINING COLOR NAVI',
            'liga_id' => 2,
            'gambar' => 'TRAINING NAVI AHHA PS PATI.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'AHHA PS PATI JERSEY TRAINING COLOR WHITE',
            'liga_id' => 2,
            'gambar' => 'TRAINING WHITE AHHA PS PATI.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'AHHA PS PATI JERSEY TRAINING COLOR YELLOW',
            'liga_id' => 2,
            'gambar' => 'TRAINING YELLOW AHHA PS PATI.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'BADAK LAMPUNG FC JERSEY HOME 2021',
            'liga_id' => 2,
            'gambar' => 'HOME BADAK LAMPUNG FC 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'BADAK LAMPUNG FC JERSEY AWAY 2021',
            'liga_id' => 2,
            'gambar' => 'AWAY BADAK LAMPUNG FC 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'BADAK LAMPUNG FC JERSEY LIGA 1 2019',
            'liga_id' => 2,
            'gambar' => 'AWAY BADAK LAMPUNG FC LIGA 1 2019.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'DEWA UNITED FC JERSEY TRAINING COLOR RED',
            'liga_id' => 2,
            'gambar' => 'TRAINING RED DEWA UNITED.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'DEWA UNITED FC JERSEY TRAINING COLOR GREEN',
            'liga_id' => 2,
            'gambar' => 'TRAINING GREEN DEWA UNITED.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'HIZBUL WATHAN JERSEY HOME 2020',
            'liga_id' => 2,
            'gambar' => 'HOME HIZBUL WATHAN 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'HIZBUL WATHAN JERSEY AWAY 2020',
            'liga_id' => 2,
            'gambar' => 'AWAY HIZBUL WATHAN 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'HIZBUL WATHAN JERSEY THIRD',
            'liga_id' => 2,
            'gambar' => 'THIRD HIZBUL WATHAN.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'KALTENG PUTRA JERSEY HOME 2020',
            'liga_id' => 2,
            'gambar' => 'HOME KALTENG PUTRA 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'KALTENG PUTRA JERSEY AWAY 2020',
            'liga_id' => 2,
            'gambar' => 'AWAY KALTENG PUTRA 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'MUBA BABEL UNITED JERSEY HOME 2019',
            'liga_id' => 2,
            'gambar' => 'HOME BABEL UNITED 2019.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'MUBA BABEL UNITED JERSEY AWAY 2019',
            'liga_id' => 2,
            'gambar' => 'AWAY BABEL UNITED 2019.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'MUBA BABEL UNITED JERSEY THIRD 2019',
            'liga_id' => 2,
            'gambar' => 'THIRD BABEL UNITED 2019.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSEKAT TEGAL JERSEY HOME 2019',
            'liga_id' => 2,
            'gambar' => 'HOME PERSEKAT 2019.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSEKAT TEGAL JERSEY AWAY 2019',
            'liga_id' => 2,
            'gambar' => 'AWAY PERSEKAT 2019.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSEKAT TEGAL JERSEY THIRD 2019',
            'liga_id' => 2,
            'gambar' => 'THIRD PERSEKAT 2019.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSEKAT TEGAL JERSEY GK HOME 2019',
            'liga_id' => 2,
            'gambar' => 'GK HOME PERSEKAT 2019.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSEKAT TEGAL JERSEY GK AWAY 2019',
            'liga_id' => 2,
            'gambar' => 'GK AWAY PERSEKAT 2019.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSEKAT TEGAL JERSEY GK THIRD 2019',
            'liga_id' => 2,
            'gambar' => 'GK THIRD PERSEKAT 2019.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSERANG SERANG JERSEY HOME 2019',
            'liga_id' => 2,
            'gambar' => 'HOME PERSERANG 2019.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSERANG SERANG JERSEY AWAY 2019',
            'liga_id' => 2,
            'gambar' => 'AWAY PERSERANG 2019.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSERANG SERANG JERSEY THIRD 2019',
            'liga_id' => 2,
            'gambar' => 'THIRD PERSERANG 2019.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSERANG SERANG JERSEY GK HOME 2019',
            'liga_id' => 2,
            'gambar' => 'GK AWAY PERSERANG 2019.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSERANG SERANG JERSEY GK AWAY 2019',
            'liga_id' => 2,
            'gambar' => 'GK HOME PERSERANG 2019.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSEWAR WAROPEN JERSEY HOME',
            'liga_id' => 2,
            'gambar' => 'HOME PERSEWAR.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSEWAR WAROPEN JERSEY HOME MODE BATIK',
            'liga_id' => 2,
            'gambar' => 'HOME MODE BATIK PERSEWAR.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSEWAR WAROPEN JERSEY HOME MODE BATIK COLOR YELLOW',
            'liga_id' => 2,
            'gambar' => 'HOME MODE BATIK YELLOW PERSEWAR.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIBA BALIKPAPAN JERSEY HOME 2008',
            'liga_id' => 2,
            'gambar' => 'HOME PERSIBA 2008.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIBA BALIKPAPAN JERSEY HOME 2011',
            'liga_id' => 2,
            'gambar' => 'HOME PERSIBA 2011.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIJAP JEPARA JERSEY HOME 2017',
            'liga_id' => 2,
            'gambar' => 'HOME PERSIJAP 2017.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIJAP JEPARA JERSEY AWAY 2019',
            'liga_id' => 2,
            'gambar' => 'AWAY PERSIJAP 2019.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIJAP JEPARA JERSEY THIRD 2019',
            'liga_id' => 2,
            'gambar' => 'THIRD PERSIJAP 2019.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIS SOLO JERSEY HOME 2021',
            'liga_id' => 2,
            'gambar' => 'HOME PERSIS 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIS SOLO JERSEY AWAY 2021',
            'liga_id' => 2,
            'gambar' => 'AWAY PERSIS 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PERSIS SOLO JERSEY HOME FANTASY PUMA 2021',
            'liga_id' => 2,
            'gambar' => 'HOME PERSIS FANTASY PUMA 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PS MITRA KUKAR TENGGARONG JERSEY HOME 2021',
            'liga_id' => 2,
            'gambar' => 'HOME MITRA KUKAR 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PS MITRA KUKAR TENGGARONG JERSEY GK HOME 2021',
            'liga_id' => 2,
            'gambar' => 'GK HOME MITRA KUKAR 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PS MITRA KUKAR TENGGARONG JERSEY HOME',
            'liga_id' => 2,
            'gambar' => 'HOME MITRA KUKAR.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PS MITRA KUKAR TENGGARONG JERSEY AWAY',
            'liga_id' => 2,
            'gambar' => 'AWAY MITRA KUKAR.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PS MITRA KUKAR TENGGARONG JERSEY THIRD',
            'liga_id' => 2,
            'gambar' => 'THIRD MITRA KUKAR.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PS MITRA KUKAR TENGGARONG JERSEY GK OLD',
            'liga_id' => 2,
            'gambar' => 'GK OLD MITRA KUKAR.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSBS BIAK JERSEY HOME',
            'liga_id' => 2,
            'gambar' => 'HOME PSBS.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSBS BIAK JERSEY HOME MODE BATIK',
            'liga_id' => 2,
            'gambar' => 'HOME MODE BATIK PSBS.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSBS BIAK JERSEY AWAY',
            'liga_id' => 2,
            'gambar' => 'AWAY PSBS.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSBS BIAK JERSEY AWAY MODE BATIK',
            'liga_id' => 2,
            'gambar' => 'AWAY MODE BATIK PSBS.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSCS CILACAP JERSEY HOME 2021',
            'liga_id' => 2,
            'gambar' => 'HOME PSCS 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSCS CILACAP JERSEY AWAY 2021',
            'liga_id' => 2,
            'gambar' => 'AWAY PSCS 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSCS CILACAP JERSEY THIRD 2021',
            'liga_id' => 2,
            'gambar' => 'THIRD PSCS 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSCS CILACAP JERSEY GK HOME 2021',
            'liga_id' => 2,
            'gambar' => 'GK HOME PSCS 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSCS CILACAP JERSEY GK AWAY 2021',
            'liga_id' => 2,
            'gambar' => 'GK AWAY PSCS 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSCS CILACAP JERSEY HOME 2020',
            'liga_id' => 2,
            'gambar' => 'HOME PSCS 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSIM MAGELANG JERSEY HOME 2021',
            'liga_id' => 2,
            'gambar' => 'HOME PSIM 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSKC CIMAHI JERSEY HOME 2020',
            'liga_id' => 2,
            'gambar' => 'HOME PSKC 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSKC CIMAHI JERSEY AWAY 2020',
            'liga_id' => 2,
            'gambar' => 'AWAY PSKC 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSKC CIMAHI JERSEY THIRD 2020',
            'liga_id' => 2,
            'gambar' => 'THIRD PSKC 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSMS MEDAN JERSEY HOME 2020',
            'liga_id' => 2,
            'gambar' => 'HOME PSMS 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSMS MEDAN JERSEY AWAY 2020',
            'liga_id' => 2,
            'gambar' => 'AWAY PSMS 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSMS MEDAN JERSEY HOME 2007',
            'liga_id' => 2,
            'gambar' => 'HOME PSMS 2007.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSMS MEDAN JERSEY AWAY 2007',
            'liga_id' => 2,
            'gambar' => 'AWAY PSMS 2007.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSMS MEDAN JERSEY HOME 1996',
            'liga_id' => 2,
            'gambar' => 'HOME PSMS 1996.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'PSPS RIAU JERSEY HOME 2018',
            'liga_id' => 2,
            'gambar' => 'HOME PSPS 2018.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'RANS CILEGON FC JERSEY HOME 2021',
            'liga_id' => 2,
            'gambar' => 'HOME RANS 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'RANS CILEGON FC JERSEY AWAY 2021',
            'liga_id' => 2,
            'gambar' => 'AWAY RANS 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'RANS CILEGON FC JERSEY THIRD 2021',
            'liga_id' => 2,
            'gambar' => 'THIRD RANS 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'RANS CILEGON FC JERSEY GK 2021',
            'liga_id' => 2,
            'gambar' => 'GK RANS 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'RANS CILEGON FC JERSEY FANTASY PUMA 2021',
            'liga_id' => 2,
            'gambar' => 'RANS FANTASY PUMA 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'SEMEN PADANG JERSEY HOME 2021',
            'liga_id' => 2,
            'gambar' => 'HOME SEMEN PADANG 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'SEMEN PADANG JERSEY AWAY 2021',
            'liga_id' => 2,
            'gambar' => 'AWAY SEMEN PADANG 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'SRIWIJAYA FC JERSEY HOME 2020',
            'liga_id' => 2,
            'gambar' => 'HOME SRIWIJAYA 2020.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'SRIWIJAYA FC JERSEY HOME',
            'liga_id' => 2,
            'gambar' => 'HOME SRIWIJAYA.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'SRIWIJAYA FC JERSEY AWAY',
            'liga_id' => 2,
            'gambar' => 'AWAY SRIWIJAYA.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'SULUT UNITED JERSEY HOME 2021',
            'liga_id' => 2,
            'gambar' => 'HOME SULUT UNITED 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'SULUT UNITED JERSEY AWAY 2021',
            'liga_id' => 2,
            'gambar' => 'AWAY SULUT UNITED 2021.png'
        ]);

        DB::table('products')->insert([
        	'nama' => 'KS TIGA NAGA JERSEY AWAY 2019',
            'liga_id' => 2,
            'gambar' => 'AWAY TIGA NAGA 2019.png'
        ]);

    }
}
